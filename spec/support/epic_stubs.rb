RSpec.shared_context 'with epic stubs context' do
  include_context 'with group stubs context'

  let(:epic) do
    {
      id: 9,
      iid: 1,
      group_id: group_id,
      parent_id: nil,
      title: 'epic title',
      description: 'epic description',
      confidential: false,
      author: {
        state: 'active',
        id: 18,
        web_url: 'https://gitlab.com/eileen.lowe',
        name: 'Alexandra Bashirian',
        avatar_url: nil,
        username: 'eileen.lowe'
      },
      start_date: nil,
      start_date_is_fixed: false,
      start_date_fixed: nil,
      start_date_from_inherited_source: nil,
      start_date_from_milestones: nil,
      end_date: nil,
      due_date: nil,
      due_date_is_fixed: false,
      due_date_fixed: nil,
      due_date_from_inherited_source: nil,
      due_date_from_milestones: nil,
      state: 'opened',
      web_url: 'https://gitlab.com/example/example/-/epics/9',
      references: {
        short: "&9",
        relative: "&9",
        full: "example/example&9"
      },
      created_at: '2021-01-07T13:46:48.348Z',
      updated_at: '2021-01-07T13:49:28.289Z',
      closed_at: nil,
      labels: [],
      upvotes: 0,
      downvotes: 0,
      _links: {
        self: 'https://gitlab.com/api/v4/groups/23/epics/9',
        epic_issues: 'https://gitlab.com/api/v4/groups/23/epics/9/issues',
        group: 'https://gitlab.com/api/v4/groups/23'
      }
    }
  end
  let(:epics) { [epic] }
end
