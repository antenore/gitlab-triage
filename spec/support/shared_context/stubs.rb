RSpec.shared_context 'with stubs context' do
  include_context 'with user stubs context'
  include_context 'with project stubs context'
  include_context 'with group stubs context'
  include_context 'with issue stubs context'
  include_context 'with merge request stubs context'
  include_context 'with epic stubs context'
end
