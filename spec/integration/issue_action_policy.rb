# frozen_string_literal: true

require 'spec_helper'

describe 'issue policy with no destination' do
  include_context 'with integration context'

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [issue]
    end
  end

  it 'creates a summary in the destination project' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Test summary
              actions:
                summarize:
                  title: New issue title
                  description: |
                    New issue description
    YAML

    stub_post_summary = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      body: { title: 'New issue title', description: 'New issue description' },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post_summary)
  end
end

describe 'issue policy with destination' do
  include_context 'with integration context'

  before do
    stub_api(
      :get,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      query: { per_page: 100 },
      headers: { 'PRIVATE-TOKEN' => token }) do
      [issue]
    end
  end

  it 'creates a summary in the destination project' do
    rule = <<~YAML
      resource_rules:
        issues:
          rules:
            - name: Test summary
              actions:
                summarize:
                  title: New issue title
                  destination: summary/destination
                  description: |
                    New issue description
    YAML

    stub_post_summary = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/summary/destination/issues",
      body: { title: 'New issue title', description: 'New issue description' },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post_summary)
  end
end
