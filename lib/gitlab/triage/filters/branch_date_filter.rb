require_relative 'base_conditions_filter'

module Gitlab
  module Triage
    module Filters
      class BranchDateFilter < BaseConditionsFilter
        ATTRIBUTES = %w[committed_date authored_date].freeze
        CONDITIONS = %w[older_than newer_than].freeze
        INTERVAL_TYPES = %w[days weeks months years].freeze

        def self.allowed_attributes
          self::ATTRIBUTES
        end

        def self.filter_parameters
          [
            {
              name: :attribute,
              type: String,
              values: allowed_attributes
            },
            {
              name: :condition,
              type: String,
              values: CONDITIONS
            },
            {
              name: :interval_type,
              type: String,
              values: INTERVAL_TYPES
            },
            {
              name: :interval,
              type: Numeric
            }
          ]
        end

        def initialize_variables(condition)
          @attribute = condition[:attribute].to_sym
          @condition = condition[:condition].to_sym
          @interval_type = condition[:interval_type].to_sym
          @interval = condition[:interval]
        end

        def resource_value
          @resource[:commit][@attribute]&.to_date
        end

        def condition_value
          @interval.public_send(@interval_type).ago.to_date # rubocop:disable GitlabSecurity/PublicSend
        end

        def calculate
          return false unless resource_value

          case @condition
          when :older_than
            resource_value < condition_value
          when :newer_than
            resource_value > condition_value
          end
        end
      end
    end
  end
end
